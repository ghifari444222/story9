from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home, log_in, log_out, signup
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class Test(TestCase):
    def test_index_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_login_url(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_login_func(self):
        found = resolve('/login')
        self.assertEqual(found.func, log_in)

    def test_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_login_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'main/login.html')

    def test_signup_url(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)

    def test_signup_func(self):
        found = resolve('/signup')
        self.assertEqual(found.func, signup)

    def test_signup_template(self):
        response = Client().get('/signup')
        self.assertTemplateUsed(response, 'main/signup.html')

    def test_login_user_success(self):
        user = User.objects.create(username='tu')
        user.set_password('tp')
        user.save()
        logged_in = Client().login(username='tu', password='tp')
        self.assertTrue(logged_in)

